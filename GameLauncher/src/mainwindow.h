#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "steam/steam_api.h"
#include <QByteArray>
#include <QDebug>
#include <QMainWindow>
#include <QMessageBox>
#include <QSysInfo>
#include <QThread>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkCookie>
#include <QtNetwork/QNetworkCookieJar>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QSslKey>
#include <QtSql/QtSql>
#ifdef _MSC_VER
#include <WS2tcpip.h>
#include <WinSock2.h>
#elif __linux__

#endif

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  void postInit();
  void testfunc();
  ~MainWindow();

public slots:

private:
  STEAM_CALLBACK(MainWindow, m_authticketresult,
                 GetAuthSessionTicketResponse_t);
  const uint32 m_steamappid = 440;
  HAuthTicket ticket;
  CSteamID steam_id;
  QByteArray tempbuffer;
  Ui::MainWindow *ui;
  QString osversion;
  QString ostype;
  QByteArray tcpstringbuffer;

  const int OSTYPEOPENSUSE = 1;
  const int OSTYPEUBUNTU = 2;
  const int OSTYPEDEBIAN = 3;
  const int OSTYPEFEDORA = 4;
  const int OSTYPEREDHAT = 5;

  QString packagename = "test";
  int processedostype;
  bool compatos = false;
  bool packageupdated = false;

  const u_short SERV_PORT = 11111;
  struct sockaddr_in6 m_servaddr;
  SOCKET sock_fd = INVALID_SOCKET;
  struct sockaddr_in m_servaddrip4;
  int errorno;
  bool sslnoerror = true;
  bool steaminitialised = false;

  bool gameready = false;

  QByteArray tcpsendbuffer;
  QByteArray tcprecvbuffer;
  byte testcharbuffer[1024];

  QString uuid;
  QNetworkCookieJar *cookiejar;
  QNetworkAccessManager *manager;

  QSslConfiguration config;
  QList<QSslCertificate> CA_Certs_List;
  QSslCertificate cert;

  QSslCertificate localcert;

  uint32 testint;
  QString m_steamserverresult;
};

#endif // MAINWINDOW_H

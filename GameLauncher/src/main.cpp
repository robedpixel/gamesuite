#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  // Note: This format must be set before show() is called.
  /*QSurfaceFormat format;
  format.setRenderableType(QSurfaceFormat::OpenGL);
  format.setProfile(QSurfaceFormat::CoreProfile);
  format.setVersion(4,5);*/
  qDebug() << "Hello";
  // Set the window up
  /*Window window;
  window.setFormat(format);
  window.resize(QSize(800, 600));
  window.show();*/
  MainWindow ywindow;
  ywindow.resize(QSize(1024, 768));
  ywindow.show();
  ywindow.postInit();
  return a.exec();
}

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  // initialise libraries
  /*if ( SteamAPI_RestartAppIfNecessary( m_steamappid ) ) // Replace with your
  App ID
  {
      exit(1);
  }*/
}

MainWindow::~MainWindow() {
  if (steaminitialised == true) {
    qDebug() << "cleaning up steam";
    SteamAPI_Shutdown();
  }
  /*if (ctx != NULL) {
    qDebug() << "cleaning up ssl ctx";
    wolfSSL_CTX_free(ctx);
  }*/
  /*if (sslinitialised == 1) {
    qDebug() << "cleaning up ssl";
    wolfSSL_Cleanup();
  }*/
  delete manager;
  delete ui;
}
void MainWindow::postInit() {
  m_steamserverresult = "";
  tcpsendbuffer.resize(4096);
  tcprecvbuffer.resize(4096);
  tempbuffer.resize(1024);
  manager = new QNetworkAccessManager(this);
  cookiejar = new QNetworkCookieJar(this);
  manager->setCookieJar(cookiejar);
  // sslinitialised = wolfSSL_Init();
  CA_Certs_List = QSslConfiguration::systemCaCertificates();
  cert = QSslCertificate::fromPath("./ca-cert.pem")[0];
  CA_Certs_List.append(cert);
  config = QSslConfiguration::defaultConfiguration();
  config.setProtocol(QSsl::TlsV1_2);
  config.setCaCertificates(CA_Certs_List);
  /*QFile key_file("./localkey.pem");
  key_file.open(QIODevice::ReadOnly);
  QSslKey sslkey(&key_file, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey);
  key_file.close();
  config.setPrivateKey(sslkey);*/

  /*qDebug() << sslinitialised;
  if (sslinitialised != 1) {
    exit(2);
  }*/
  steaminitialised = SteamAPI_Init();
  if (steaminitialised != true) {
    exit(3);
  }

  QSysInfo sysinfo;
  ostype = sysinfo.productType();
  osversion = sysinfo.productVersion();
  QString textline;
  textline.append("This tool is meant to install related game deps on linux "
                  "through interfacing with its package manager.");
  ui->textEdit->append(textline);
  textline.clear();
  textline.append("Operating system:");
  textline.append(ostype);
  ui->textEdit->append(textline);
  textline.clear();
  textline.append("Version:");
  textline.append(osversion);
  ui->textEdit->append(textline);

#ifdef _MSC_VER

  textline.clear();
  textline.append("Windows distribution detected!");
  ui->textEdit->append(textline);

  textline.clear();
  textline.append("setting dll paths...");
  ui->textEdit->append(textline);

  textline.clear();
  textline.append("test mode activated");
  ui->textEdit->append(textline);

  textline.clear();
  textline.append("steamapi test");
  ui->textEdit->append(textline);

  // authenticate with my server using steam authentication ticket
  qDebug() << steaminitialised;
  steam_id = SteamUser()->GetSteamID();
  ticket = SteamUser()->GetAuthSessionTicket(tempbuffer.data(),
                                             tempbuffer.size(), &testint);
  qDebug() << "sleeping for 2 sec";
  QThread::sleep(2);
  SteamAPI_RunCallbacks();
  // if successful,download dependency package version information

  textline.clear();
  textline.append("TEST:getting package information");

  // connect to controller server for package information (with wolfssl)
  qInfo() << "connecting to server...";
  textline.clear();
  textline.append("connecting to server..");
  ui->textEdit->append(textline);
  while (m_steamserverresult.isEmpty()) {
    QThread::msleep(500);
    qDebug() << "waiting...";
  }
  if (m_steamserverresult.toInt() == k_EBeginAuthSessionResultOK) {
    qDebug() << "connection successful";
    textline.clear();
    textline.append("connection successful");
    ui->textEdit->append(textline);
  } else {
    qDebug() << "connection failed";
    textline.clear();
    textline.append("connection failed");
    ui->textEdit->append(textline);
  }
  // testfunc works
  qDebug() << "download test file";
  testfunc();
  // TODO end auth

  QNetworkReply *reply = nullptr;
  QUrl params;
  QNetworkRequest request;
  QUrlQuery query;
  QString tempsteamid;
  tempsteamid.setNum(steam_id.ConvertToUint64());
  QString string = tempbuffer.toHex();
  query.addQueryItem("ticket", string);
  query.addQueryItem("steamID", tempsteamid);
  query.addQueryItem("uuid", uuid);
  query.addQueryItem("size", QString::number(testint));
  params.setScheme("https");
  params.setHost("localhost");
  params.setPort(3000);
  params.setPath("/steam/end");
  params.setQuery(query);

  request.setSslConfiguration(config);

  request.setUrl(params);
  request.setHeader(QNetworkRequest::ContentTypeHeader,
                    "application/x-www-form-urlencoded");
  QEventLoop loop;
  reply = manager->post(request, query.query(QUrl::FullyEncoded).toUtf8());
  connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
  loop.exec();
  reply->deleteLater();
  reply = nullptr;
  qDebug() << "finished connection";
  qDebug() << "enabling dlls";
  LPCWSTR temptxt = QDir::currentPath().toStdWString().c_str();
  SetDllDirectory(temptxt);
  qDebug() << "ready to play!";
  ui->pushButton->setEnabled(true);
#endif

#ifdef __linux__
  textline.clear();
  textline.append("Supported linux distributions:");
  textline.append("ubuntu,debian,opensuse,fedora,redhat");
  ui->textEdit->append(textline);
  // check for compatible linux version
  if (ostype.contains("opensuse", Qt::CaseInsensitive) == true) {
    processedostype = OSTYPEOPENSUSE;
    ostype = "opensuse";
    compatos = true;
  } else if (ostype.contains("ubuntu", Qt::CaseInsensitive) == true) {
    processedostype = OSTYPEUBUNTU;
    ostype = "ubuntu";
    compatos = true;
  } else if (ostype.contains("debian", Qt::CaseInsensitive) == true) {
    processedostype = OSTYPEDEBIAN;
    ostype = "debian";
    compatos = true;
  } else if (ostype.contains("fedora", Qt::CaseInsensitive) == true) {
    processedostype = OSTYPEFEDORA;
    ostype = "fedora";
    compatos = true;
  } else if (ostype.contains("red hat", Qt::CaseInsensitive) == true) {
    processedostype = OSTYPEREDHAT;
    ostype = "redhat";
    compatos = true;
  } else if (ostype.contains("windows", Qt::CaseInsensitive) == true) {
    compatos = false;
  } else {
    qWarning() << "Linux distribution not detected! Launcher is running on an "
                  "unknown distribution or wrong operating system!";
  }
  if (compatos == true) {
    // authenticate with my server using steam authentication ticket
    qWarning() << "steamapi test:";
    uint32 testint;
    tempbuffer.resize(1024);
    steam_id = SteamUser()->GetSteamID();
    ticket = SteamUser()->GetAuthSessionTicket(tempbuffer.data(),
                                               tempbuffer.size(), &testint);
    QThread::sleep(2);
    SteamAPI_RunCallbacks();
    // check if downloaded package version matches
    while (m_steamserverresult.isEmpty()) {
      QThread::msleep(500);
      qDebug() << "waiting...";
    }
    if (m_steamserverresult.toInt() == k_EBeginAuthSessionResultOK) {
      qDebug() << "connection successful";
      textline.clear();
      textline.append("connection successful");
      ui->textEdit->append(textline);
    } else {
      qDebug() << "connection failed";
      textline.clear();
      textline.append("connection failed");
      ui->textEdit->append(textline);
    }
    // if successful,download dependency package version information
    qInfo() << "getting package information";
    QNetworkReply *reply = nullptr;
    QUrl params;
    QNetworkRequest request;
    QUrlQuery query;
    QString tempsteamid;
    tempsteamid.setNum(steam_id.ConvertToUint64());
    QString string = tempbuffer.toHex();
    query.addQueryItem("os", ostype);
    params.setScheme("http");
    params.setHost("localhost");
    params.setPort(3000);
    params.setPath("/package/get");
    params.setQuery(query);
    request.setUrl(params);
    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      "application/x-www-form-urlencoded");
    QEventLoop loop;
    reply = manager->get(request);
    connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
    loop.exec();
    QByteArray replyData = reply->readAll();
    QJsonDocument jsonDoc(QJsonDocument::fromJson(replyData));
    QJsonObject jsonReply = jsonDoc.object();
    latestpackageversion = jsonDoc["version"].toString();
    reply->deleteLater();
    reply = nullptr;
    // if not present or updated, download package from server
    qInfo() << "checking if package is present";
    textline.clear();
    textline.append("checking if package is present");
    ui->textEdit->append(textline);
    if (processedostype == OSTYPEUBUNTU || processedostype == OSTYPEDEBIAN) {
      QProcess process;
      process.start("dpkg", QStringList() << "-s" << packagename << "|"
                                          << "grep"
                                          << "-n"
                                          << "-C2" << packagename);
      process.waitForFinished();
      QString output(process.readAllStandardOutput());
      qDebug() << output;
      // check if package is updated
      if (output.contains(packagename)) {
        // read version info
      }

    } else if (processedostype == OSTYPEOPENSUSE ||
               processedostype == OSTYPEFEDORA ||
               processedostype == OSTYPEREDHAT) {
      QProcess process;
      process.start("rpm", QStringList() << "-q" << packagename);
      process.waitForFinished();
      QString output(process.readAllStandardOutput());
      qDebug() << output;
      ui->textEdit->append(output);
      // check if package is updated

    } else {
    }

    // if not updated, download package from server to gamelauncher_dir/temp
    if (packageupdated == false) {
      if (processedostype == OSTYPEUBUNTU) {

      } else if (processedostype == OSTYPEDEBIAN) {

      } else {
      }
    }
    qInfo() << "connecting to server...";
  }
#endif
  /*textline.clear();
  textline.append(tempbuffer);
  ui->textEdit->append(textline);*/
}
void MainWindow::testfunc() {
  // get download and load it into download folder
  QNetworkReply *reply = nullptr;
  QUrl params;
  QNetworkRequest request;
  QUrlQuery query;
  QString tempsteamid;
  tempsteamid.setNum(steam_id.ConvertToUint64());
  QString string = tempbuffer.toHex();
  query.addQueryItem("file", "latest");
  query.addQueryItem("os", "testos");
  params.setScheme("https");
  params.setHost("localhost");
  params.setPort(3000);
  params.setPath("/download");
  params.setQuery(query);

  request.setSslConfiguration(config);

  request.setUrl(params);
  request.setHeader(QNetworkRequest::ContentTypeHeader,
                    "application/x-www-form-urlencoded");
  QEventLoop loop;
  reply = manager->get(request);
  connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
  loop.exec();
  // reply is empty for some reason
  QByteArray filearray = reply->readAll();
  if (filearray.isEmpty() == false) {
    qDebug() << "writing download file";
    QFile testfile(QDir::currentPath() + "/testfile.txt");
    testfile.open(QIODevice::WriteOnly);
    testfile.write(filearray);
    testfile.close();
  }
  reply->deleteLater();
  reply = nullptr;
}
void MainWindow::m_authticketresult(GetAuthSessionTicketResponse_t *pCallback) {
  // tcpstringbuffer.append("AUTHENTICATE:");
  tempbuffer.resize(testint);
  // tcpstringbuffer.append(QString::number(tempbuffer.toInt()).toUtf8());
  qDebug() << "size:" << testint;
  qDebug() << "ticket: " << tempbuffer;
  qDebug() << steam_id.ConvertToUint64();
  QNetworkRequest request;
  QNetworkReply *reply = nullptr;

  request.setSslConfiguration(config);

  QString tempsteamid;
  tempsteamid.setNum(steam_id.ConvertToUint64());
  QUrl params;
  QUrlQuery query;
  QString string = tempbuffer.toHex();
  query.addQueryItem("ticket", string);
  query.addQueryItem("steamID", tempsteamid);
  query.addQueryItem("size", QString::number(testint));
  params.setScheme("https");
  params.setHost("localhost");
  params.setPort(3000);
  params.setPath("/steam/authenticate");
  params.setQuery(query);
  request.setUrl(params);
  request.setHeader(QNetworkRequest::ContentTypeHeader,
                    "application/x-www-form-urlencoded");
  QEventLoop loop;
  /*connect(manager, &QNetworkAccessManager::finished, this,
          &MainWindow::HandleNetworkData);*/
  // manager->connectToHost("localhost",3000);
  reply = manager->post(request, query.query(QUrl::FullyEncoded).toUtf8());
  connect(reply, SIGNAL(finished()), &loop, SLOT(quit()));
  loop.exec();
  QByteArray replyData = reply->readAll();
  QJsonDocument jsonDoc(QJsonDocument::fromJson(replyData));
  QJsonObject jsonReply = jsonDoc.object();
  reply->deleteLater();
  reply = nullptr;
  m_steamserverresult = jsonDoc.object().value("status").toString();
  qInfo() << m_steamserverresult;
  /*errorno = ::connect(sock_fd, reinterpret_cast<struct
  sockaddr*>(&m_servaddr), sizeof(m_servaddr)); if (errorno == -1){
      qDebug()<<"error connecting to socket";
      closesocket(sock_fd);
      exit( EXIT_FAILURE);
  }
  wolfSSL_set_fd(ssl, sock_fd);
  errorno = wolfSSL_connect(ssl);
  if (errorno == -1){
      qDebug()<<"error connecting ssl to socket";
      wolfSSL_free(ssl);
      shutdown(sock_fd,SD_SEND);
      closesocket(sock_fd);
      exit( EXIT_FAILURE);
  }
  //something is wrong with the size of the hexified string
  errorno = wolfSSL_write(ssl,tcpsendbuffer.data(), tcpsendbuffer.size());*/
  /*QThread::sleep(1);
  wolfSSL_read(ssl,tcprecvbuffer.data(),tcprecvbuffer.size());
      tcpstringbuffer.clear();
      tcpstringbuffer.append(tcpbuffer.toHex());
      qDebug()<<tcpstringbuffer;*/
  /* shutdown(sock_fd,SD_SEND);
   closesocket(sock_fd);
   SteamUser()->CancelAuthTicket(ticket);*/
}

#ifndef TESTFUNCTIONS_H
#define TESTFUNCTIONS_H
class TestFunctions {
public:
  TestFunctions();
  bool testTrue();
  bool testFalse();
  // always returns 4
  int fakeadd(int a, int b);
  int realadd(int a, int b);
};

#endif // TESTFUNCTIONS_H

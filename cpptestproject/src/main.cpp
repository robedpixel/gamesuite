#define BOOST_TEST_MODULE MyTest
#include "testfunctions.h"
#include <fstream>
#include <gtest/gtest.h>

TEST(BasicTest, TestTrue) {
  TestFunctions test;
  EXPECT_EQ(true, test.testTrue());
}
TEST(BasicTest, TestFalse) {
  TestFunctions test;
  EXPECT_EQ(false, test.testFalse());
}
TEST(BasicTest, TestFakeAdd) {
  TestFunctions test;
  EXPECT_EQ(4, test.fakeadd(2, 2));
}

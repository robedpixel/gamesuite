#include "steam.h"

using namespace Cutelyst;

steam::steam(QObject *parent,QSqlDatabase* db) : Controller(parent)
{
    m_db_ptr = db;
}

steam::~steam()
{
}
//all functions locked behind login funcs should authenticate ticket before carrying out function
void steam::index(Context *c)
{
    /*c->response()->body() = "Welcome to steam!";*/
}
void steam::index_GET(Context *c) {
	const QString testarg = c->request()->queryParam("testarg");
	c->response()->setJsonObjectBody({
											  {QStringLiteral("status"), testarg},
		});
}
void steam::index_authenticate(Context *c) {

}
void steam::index_authenticate_POST(Context *c){
    const QString ticket = c->request()->bodyParam("ticket");
    qDebug()<<ticket;
    CSteamID testid;
    const QString steam_id = c->request()->bodyParam("steamID");
    qDebug()<<steam_id;
	const int size = c->request()->bodyParam("size").toInt();
	qDebug() << size;
    if (!ticket.isEmpty() && !steam_id.isEmpty()) {
        //convert steam_id to uint64
        testid.SetFromUint64(steam_id.toULongLong());
		qDebug() << "authenticating...";
        EBeginAuthSessionResult result;
		QByteArray ticketdata;
    //convert hex string ticket to a bytearray, then decode it to binary
		ticketdata = QByteArray::fromHex(ticket.toLatin1());
		qDebug() << "buffersize:"<<ticket.size();
		qDebug() << ticketdata;
        result=SteamGameServer()->BeginAuthSession(ticketdata.data(), size, testid);
		qDebug() << result;


        if (result == k_EBeginAuthSessionResultOK){
            //store user ticket and steamid in database,get index id and pass it into cookiejar along with steamkey
			qDebug() << "checking for duplicates";
			{
				QSqlQuery tempQuery;
				int64_t longsteamid = static_cast<int64_t>(steam_id.toULongLong());
				tempQuery.prepare("SELECT uuid FROM users WHERE steamid = :steamid");
				tempQuery.bindValue(":steamid", longsteamid);
				if (tempQuery.exec() == true) {
					if (tempQuery.next()==true) {
						//duplicate found, delete duoplicate
						qDebug() << "duplicates found";
						tempQuery.finish();
						tempQuery.prepare("DELETE FROM users WHERE steamid = :steamid");
						tempQuery.bindValue(":steamid", steam_id);
						if (tempQuery.exec() == true) {
							qDebug() << "deleted duplicates";
						}
						else {
							qDebug() << "database error deleting douplicates";
							m_db_ptr->rollback();
						}
					}
					else {
						//no duplicate found
					}
				}
				else {
					qDebug() << "database error finding duplicates";
					m_db_ptr->rollback();
				}
			}
			qDebug() << "loading into database";
            uuid = QUuid::createUuid();
			int64_t longsteamid = static_cast<int64_t>(testid.ConvertToUint64());
            QSqlQuery newQuery;
            newQuery.prepare("INSERT INTO users (uuid, ticket, steamid) VALUES (:uuid , :ticket, :steamid) RETURNING uuid");
            newQuery.bindValue(":uuid",uuid.toString());
            newQuery.bindValue(":ticket",ticket);
            newQuery.bindValue(":steamid",longsteamid);

            if (newQuery.exec() == true) {
                //total success result
                newQuery.next();
                QString queryuuid = newQuery.value(0).toString();
                qDebug() << queryuuid;
                Session::setValue(c,"sessionid",queryuuid);
                Session::setValue(c,"ticket",ticket);
				Session::setValue(c, "steamid", steam_id);
				QString stringresult = QString::number(result);
				qDebug() << stringresult;
                c->response()->setJsonObjectBody({
                                                       {QStringLiteral("status"), QStringLiteral("success")},
                                                       {QStringLiteral("result"), QString::number(result)}
                });

            }
            else {
                //rollback and error
				qDebug() << "database error";
                m_db_ptr->rollback();
                c->response()->setJsonObjectBody({
                                                       {QStringLiteral("status"), QStringLiteral("database error")},
                                                       {QStringLiteral("result"), QString::number(result)}
                });
            }
        }else{
            c->response()->setJsonObjectBody({
                                                   {QStringLiteral("status"), QStringLiteral("error")},
                                                   {QStringLiteral("result"), QString::number(result)}
            });
        }
    }
    else {
        c->response()->setJsonObjectBody({
                                               {QStringLiteral("status"), QStringLiteral("error in args")}

        });
    }
}
void steam::index_end(Context *c) {

}
void steam::index_end_POST(Context *c) {
	const QString ticket = c->request()->bodyParam("ticket");
	qDebug() << ticket;
	QVariant steamidkey = Session::value(c, "steamid");
	qDebug() << "steamidkey:" << steamidkey;
	QVariant ticketkey = Session::value(c, "ticket");
	qDebug() << "ticketkey:" << ticketkey;
    QVariant sessionkey=Session::value(c,"sessionid");
	qDebug() << "sessionkey:" << sessionkey;
    QHostAddress clientaddress = c->request()->address();
	qDebug() << clientaddress.toString();
	//check if client is authenticated, if it is, close auth and delete entry in database
    QSqlQuery newQuery;
    newQuery.prepare("SELECT * FROM users WHERE uuid = :uuid");
    newQuery.bindValue(":uuid",sessionkey.toString());

    if (newQuery.exec() == true) {
        newQuery.next();
        QString ticketstring = newQuery.value(1).toString();
        QString steamidstring = newQuery.value(2).toString();
        if (QString::compare(ticketkey.toString(), ticketstring, Qt::CaseInsensitive)==0){
            //close auth then delete entry
            CSteamID endauthid;
            endauthid.SetFromUint64(steamidstring.toULongLong());
            qDebug()<<"ending steam auth";
            SteamGameServer()->EndAuthSession(endauthid);
            qDebug()<<"deleting token";
            newQuery.prepare("DELETE FROM users WHERE uuid = :uuid");
            newQuery.bindValue(":uuid",sessionkey.toString());
            if (newQuery.exec() == true){
				qDebug() << "ending session";
				Session::deleteSession(c, "session expired");
                c->response()->setJsonObjectBody({
                                                     {QStringLiteral("status"), QStringLiteral("success")}
                });
            }
            else{
                c->response()->setJsonObjectBody({
                                                     {QStringLiteral("status"), QStringLiteral("database error")}
                });
            }
        }
    }
    else {
        m_db_ptr->rollback();
        c->response()->setJsonObjectBody({
                                               {QStringLiteral("status"), QStringLiteral("database error")}
        });
    }
}

void steam::index_status(Context *c){

}
void steam::index_status_GET(Context *c){

}

#ifndef SERVERREST_H
#define SERVERREST_H

#include <Cutelyst/Application>
#include <Cutelyst/Plugins/Session/Session>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QDebug>
#include <QDir>
#include "steam/steam_gameserver.h"
#ifdef _MSC_VER
	#include <WS2tcpip.h>
#elif __linux__

#endif

#include "steam.h"
#include "root.h"
#include "package.h"
#include "download.h"
using namespace Cutelyst;

class ServerREST : public Application
{
    Q_OBJECT
    CUTELYST_APPLICATION(IID "ServerREST")
public:
    Q_INVOKABLE explicit ServerREST(QObject *parent = nullptr);
    ~ServerREST();

    bool init();
private:
	QSqlDatabase m_db;
	bool m_err;
};

#endif //SERVERREST_H

#include "helperclass.h"

helperclass::helperclass() {}
bool helperclass::isauthenticated(Context *c, QSqlDatabase *db) {
  if (Session::value(c, "sessionid").isNull() == true) {
    return false;
  }
  if (Session::value(c, "ticket").isNull() == true) {
    return false;
  }
  if (Session::value(c, "steamid").isNull() == true) {
    return false;
  }
  QSqlQuery newQuery;
  newQuery.prepare("SELECT * FROM users WHERE uuid = :uuid");
  newQuery.bindValue(":uuid", Session::value(c, "sessionid"));
  if (newQuery.exec() == true) {
    newQuery.next();
    QString querystringticket = newQuery.value(1).toString();
    QString querystringid = newQuery.value(2).toString();
    if (querystringticket.compare(Session::value(c, "ticket").toString(),
                                  Qt::CaseSensitive) == 0) {
      if (querystringid.compare(Session::value(c, "steamid").toString(),
                                Qt::CaseSensitive) == 0) {
		  qDebug() << "authentication successful";
        return true;
      }
    }
  } else {
    db->rollback();
    return false;
  }
  return false;
}

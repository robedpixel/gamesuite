#include "package.h"

using namespace Cutelyst;

package::package(QObject *parent, QSqlDatabase *db) : Controller(parent) {
  m_db_ptr = db;
}

package::~package() {}

void package::index(Context *c) {
  c->response()->body() = "Matched Controller::package in package.";
}
void package::index_get(Context *c) {}
void package::index_get_GET(Context *c) {
  if (helperclass::isauthenticated(c, m_db_ptr) == true) {
    QString ostype = c->request()->queryParam("os");
    QSqlQuery newQuery;
    newQuery.prepare(
        "SELECT package_version FROM testtable WHERE os_type = :os_type?");
    newQuery.bindValue("os_type", ostype);
    if (newQuery.exec() == true) {
      newQuery.next();
      QString querystring = newQuery.value(0).toString();
      qDebug() << querystring;
      c->response()->setJsonObjectBody(
          {{QStringLiteral("ostype"), ostype},
           {QStringLiteral("version"), querystring}});
    } else {
      m_db_ptr->rollback();
    }
  } else {
    c->response()->setStatus(404);
  }
}

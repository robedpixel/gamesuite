#ifndef DOWNLOAD_H
#define DOWNLOAD_H

#include "helperclass.h"
#include <Cutelyst/Controller>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
using namespace Cutelyst;

class download : public Controller {
  Q_OBJECT
public:
  explicit download(QObject *parent = nullptr, QSqlDatabase *db = nullptr);
  ~download();

  C_ATTR(index, : Path:AutoArgs : ActionClass(REST))
  void index(Context *c);
  C_ATTR(index_GET, : private)
  void index_GET(Context *c);
  C_ATTR(index_POST, : private)
  void index_POST(Context *c);

private:
  QSqlDatabase *m_db_ptr;
};

#endif // DOWNLOAD_H

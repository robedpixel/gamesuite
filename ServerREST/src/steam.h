#ifndef STEAM_H
#define STEAM_H

#include <Cutelyst/Controller>
#include <Cutelyst/Plugins/Session/Session>
#include <QtNetwork/QHostAddress>
#include <QtSql/QSqlQuery>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QUuid>
#include "steam/steam_gameserver.h"
using namespace Cutelyst;

class steam : public Controller
{
    Q_OBJECT
public:
    explicit steam(QObject *parent = nullptr,QSqlDatabase* db=nullptr);
    ~steam();

    C_ATTR(index, :Path :AutoArgs :ActionClass(REST))
    void index(Context *c);
    C_ATTR(index_GET, :private)
    void index_GET(Context *c);
    C_ATTR(index_authenticate, :Path('authenticate') :AutoArgs :ActionClass(REST))
	void index_authenticate(Context *c);
    C_ATTR(index_authenticate_POST, :private)
    void index_authenticate_POST(Context *c);
	C_ATTR(index_end, :Path('end') : AutoArgs : ActionClass(REST))
	void index_end(Context *c);
	C_ATTR(index_status_GET, :private)
	void index_end_POST(Context *c);
    C_ATTR(index_status, :Path('status') : AutoArgs :ActionClass(REST))
    void index_status(Context *c);
    C_ATTR(index_status_GET, :private)
    void index_status_GET(Context *c);
private:
    QSqlDatabase* m_db_ptr;
    QUuid uuid;
};

#endif //STEAM_H


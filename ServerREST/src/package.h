#ifndef PACKAGE_H
#define PACKAGE_H

#include "helperclass.h"
#include "steam/steam_gameserver.h"
#include <Cutelyst/Controller>
#include <Cutelyst/Plugins/Session/Session>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtNetwork/QHostAddress>
#include <QtSql/QSqlQuery>
using namespace Cutelyst;

class package : public Controller {
  Q_OBJECT
public:
  explicit package(QObject *parent = nullptr, QSqlDatabase *db = nullptr);
  ~package();

  C_ATTR(index, : Path : AutoArgs)
  void index(Context *c);
  C_ATTR(index_get, : Path('get') : AutoArgs : ActionClass(REST))
  void index_get(Context *c);
  C_ATTR(index_get_GET, : private)
  void index_get_GET(Context *c);

private:
  QSqlDatabase *m_db_ptr;
};

#endif // PACKAGE_H

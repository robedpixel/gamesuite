#ifndef HELPERCLASS_H
#define HELPERCLASS_H

#include <Cutelyst/Controller>
#include <Cutelyst/Plugins/Session/Session>
#include <QtSql/QSqlQuery>
#include <QDebug>
using namespace Cutelyst;

class helperclass {
public:
  helperclass();
  static bool isauthenticated(Context *c, QSqlDatabase *db);
};

#endif // HELPERCLASS_H

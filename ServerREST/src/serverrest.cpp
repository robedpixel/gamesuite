#include "serverrest.h"


using namespace Cutelyst;

ServerREST::ServerREST(QObject *parent) : Application(parent)
{
}

ServerREST::~ServerREST()
{
	if(m_db.isOpen()){
		m_db.close();
	}
	SteamGameServer_Shutdown();
}

bool ServerREST::init()
{
    qDebug() << "App path : " << QDir::currentPath();
    m_db = QSqlDatabase::addDatabase("QPSQL");
    m_db.setHostName("localhost");
    m_db.setDatabaseName("testtable");
    m_db.setUserName("postgres");
    m_db.setPassword("testadmin");
    bool ok = m_db.open();
    if (ok) {
        qDebug() << "connected to database!";
    }
    else{
        qDebug() << "cannot connect to database";
        exit(1);
    }
    m_err = SteamGameServer_Init(INADDR_ANY,27015,27016,27017,eServerModeAuthenticationAndSecure,"1.0.0.0");
	if (m_err) {
        qDebug() << "connected to steam game server!";
	}
	else {
        qDebug() << "error connecting to steam game server!";
	}
    SteamGameServer()->LogOnAnonymous();
    new Session(this);
    new Root(this);
	new steam(this,&m_db);
	new package(this,&m_db);
	new download(this,&m_db);
    return true;
}

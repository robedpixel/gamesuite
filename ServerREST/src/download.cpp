#include "download.h"

using namespace Cutelyst;

download::download(QObject *parent, QSqlDatabase *db) : Controller(parent) {
  m_db_ptr = db;
}

download::~download() {}

void download::index(Context *c) {
  c->response()->body() = "Matched Controller::download in download.";
}
void download::index_GET(Context *c) {
  if (helperclass::isauthenticated(c, m_db_ptr) == true) {
    // code crashes somewhere here
    QFile downloadfile;
    QString latestfile;
    QString filetype = c->request()->queryParam("file");
    if (filetype.compare("latest", Qt::CaseInsensitive) == 0) {
      QString ostype = c->request()->queryParam("os");
      QSqlQuery newQuery;
      newQuery.prepare("SELECT * FROM testtable WHERE os_type = :os_type");
      newQuery.bindValue(":os_type", ostype);
      if (newQuery.exec() == true) {
        newQuery.next();
        latestfile = newQuery.value(1).toString();
      } else {
        m_db_ptr->rollback();
      }
      qDebug() << latestfile;
      QString downloadfilepath =
          QDir::currentPath() + "/download/" + latestfile;
      downloadfile.setFileName(downloadfilepath);
    } else {
      QString downloadfilepath =
          QDir::currentPath() + "/download/" + c->request()->queryParam("file");
      downloadfile.setFileName(downloadfilepath);
    }
    if (downloadfile.exists() == true) {
      qDebug() << downloadfile.fileName();
      downloadfile.open(QIODevice::ReadOnly);
      QByteArray data = downloadfile.readAll();
      c->response()->setBody(data);
      downloadfile.close();
    } else {
      c->response()->setJsonObjectBody({
          {QStringLiteral("status"), QStringLiteral("error finding file")},

      });
    }
  } else {
    c->response()->setStatus(404);
  }
}
// make post method that is same as get but send as json to test with insomnia
// rest client
void download::index_POST(Context *c) {
  QFile downloadfile;
  QString latestfile;
  QString filetype = c->request()->queryParam("file");
  if (filetype.compare("latest", Qt::CaseInsensitive) == 0) {
    QString ostype = c->request()->queryParam("os");
    QSqlQuery newQuery;
    newQuery.prepare("SELECT * FROM testtable WHERE os_type = :os_type");
    newQuery.bindValue(":os_type", ostype);
    if (newQuery.exec() == true) {
      newQuery.next();
      latestfile = newQuery.value(1).toString();
    } else {
      m_db_ptr->rollback();
    }
    qDebug() << latestfile;
    QString downloadfilepath = QDir::currentPath() + "/download/" + latestfile;
    downloadfile.setFileName(downloadfilepath);
  } else {
    QString downloadfilepath =
        QDir::currentPath() + "/download/" + c->request()->queryParam("file");
    downloadfile.setFileName(downloadfilepath);
  }
  if (downloadfile.exists() == true) {
    qDebug() << downloadfile.fileName();

    downloadfile.open(QIODevice::ReadOnly);
    QByteArray data = downloadfile.readAll();
    QString hexdata = data.toHex();
    c->response()->setJsonObjectBody({
        {QStringLiteral("data"), hexdata},

    });
    downloadfile.close();
  } else {
    c->response()->setJsonObjectBody({
        {QStringLiteral("status"), QStringLiteral("error finding file")},

    });
  }
}

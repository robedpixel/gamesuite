# create this function in the calling script
function Get-ScriptDirectory { Split-Path $MyInvocation.ScriptName }

# generate the path to the script in the utils directory:build\src
$serverpath = Join-Path (Get-ScriptDirectory) 'build\src'

$port = 'localhost:3000'
$ServerCert = Join-Path (Get-ScriptDirectory) 'extras\47109155_localhost.crt'
$ServerKey = Join-Path (Get-ScriptDirectory) 'extras\47109155_localhost.key'
$servernetcmd = $port +','+$ServerCert+','+$ServerKey

Set-Location $serverpath
Start-Process -FilePath "C:/usr/bin/cutelystd3-qt6" -WorkingDirectory $serverpath -ArgumentList "--master","-a",".\ServerREST.dll","--hs1",$servernetcmd
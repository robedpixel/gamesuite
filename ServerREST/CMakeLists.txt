project(ServerREST)

cmake_minimum_required(VERSION 3.28 FATAL_ERROR)
if (POLICY CMP0043)
  cmake_policy(SET CMP0043 NEW)
endif()

#build tool optimisation
if ("${CMAKE_GENERATOR}" MATCHES "NMake Makefiles")
  add_compile_options("/MP")
endif()

#compiler settings
if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
  message("USING CLANG")
  if (WIN32)
      set(CMAKE_LINKER "C:/Program Files/LLVM/bin/lld-link.exe")
      set(CMAKE_RANLIB "C:/Program Files/LLVM/bin/llvm-ranlib.exe")
      set(CMAKE_AR "C:/Program Files/LLVM/bin/llvm-ar.exe")
      if(MSVC)
        add_compile_options("/W3")
        add_compile_options("$<$<CONFIG:RELEASE>:/O2;/clang:-fstack-protector;/clang:-finline-functions>")
      endif()
  elseif (UNIX)
      add_compile_options("$<$<CONFIG:RELEASE>:-O;-stdlib=libstdc++;-fPIE;-fstack-protector;-finline-functions;-fPIC>")
  endif()
  set(CMAKE_CXX_STANDARD 20)
  set(CMAKE_CXX_STANDARD_REQUIRED ON)
  add_compile_options("$<$<CONFIG:RELEASE>:-mretpoline;-fcf-protection;-flto;-mf16c;-mavx;-mavx2;-mrdrnd;-mrdseed;-maes;-mpclmul;-mfma;-msha>")
elseif(MSVC)
  message("USING MSVC")
  add_compile_options("$<$<CONFIG:RELEASE>:/Qspectre;/sdl;/O2;/Oi;/Ob2;/GT;/GL;/GS;/guard:cf;/arch:AVX2;/MD>" )
endif()

# set library install paths
set(Cutelyst3Qt6_DIR "C:/usr/lib/cmake/Cutelyst3Qt6")

find_package(Qt6 COMPONENTS Core Network Sql REQUIRED)
find_package(Cutelyst3Qt6 REQUIRED)

# Auto generate moc files
set(CMAKE_AUTOMOC ON)

# As moc files are generated in the binary dir, tell CMake
# to always look for includes there:
set(CMAKE_INCLUDE_CURRENT_DIR ON)

file(GLOB_RECURSE TEMPLATES_SRC root/*)
add_subdirectory(src)
